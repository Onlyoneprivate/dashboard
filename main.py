from dash import Dash, dcc, html, Input, Output, State
from dash.dependencies import Input, Output, ALL, State, MATCH, ALLSMALLER
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas as pd
import numpy as np


app = Dash(__name__, external_stylesheets=[dbc.themes.SKETCHY])
df = pd.read_excel('data\ปริมาณของกลางยาเสพติด.xlsx')
colors = {
    'text' : '#BBDFGT',
    'plot_colors' : '#FF99CC' ,#สีพื้นหลังกราฟ
    'paper_colors' : '#6699FF' #สีกระดาษ
}
df.columns = [
            'ปีงบประมาณ', 
            'จังหวัด', 
            'สารเสพติด',
            'ยาบ้า (เม็ด)',
            'ไอซ์ (กิโลกรัม)',
            'กัญชาแห้ง (กิโลกรัม)',
            'เฮโรอีน (กิโลกรัม)',
            'คีตามีน (กิโลกรัม)',
            'โคเคน (กิโลกรัม)',
            'กระท่อม (กิโลกรัม)'
            ]


@app.callback(
            Output("pie-graph","figure"), 
            Input('ปีงบประมาณ','value'),
            Input("จังหวัด", "value"),
            Input("สารเสพติด", "value"),
            
            )

def update_graph(selected_year, province, chart_choice):
    filtted_data = df[df['ปีงบประมาณ'] == int(selected_year)] [df["จังหวัด"].str.contains(province)]

    if chart_choice == 'scatter':
        fig = px.scatter(
            filtted_data,
            x = "จังหวัด", 
            y = "สารเสพติด", 
            color = ['ยาบ้า (เม็ด)',
            'ไอซ์ (กิโลกรัม)',
            'กัญชาแห้ง (กิโลกรัม)',
            'เฮโรอีน (กิโลกรัม)',
            'คีตามีน (กิโลกรัม)',
            'โคเคน (กิโลกรัม)',
            'กระท่อม (กิโลกรัม)'],
            title = f'แผนภูมิจุดแบบกระจัดกระจายแสดงคะแนนเฉลี่ย O-NET ปี {selected_year} ของพื้นที่จังหวัด {province}'
            ).update_layout(
                {"plot_bgcolor": '#e5da6c', "paper_bgcolor": '#40ba9e'}  
                            )

        return fig

    elif chart_choice == 'pie':
        fig = px.pie(filtted_data,
                    values = 'สารเสพติด', 
                    names = 'จังหวัด',
                    title=f'ปริมาณของกลางยาเสพติดของปี {selected_year} ของจังหวัด {province}',
                    color = 'จังหวัด',
                    color_discrete_map={'ยาบ้า (เม็ด)':'#FF99CC',
                                        'ไอซ์ (กิโลกรัม)':'#6699FF',
                                        'กัญชาแห้ง (กิโลกรัม)':'#CC9999',
                                        'เฮโรอีน (กิโลกรัม)':'#CC9966',
                                        'คีตามีน (กิโลกรัม)': '#9999FF',
                                        'โคเคน (กิโลกรัม)': '#9999CC',
                                        'กระท่อม (กิโลกรัม)': '#999966'
                                        }).update_layout(
                        {'plot_colors': colors['plot_colors'] , 'paper_colors' : colors['paper_colors']}
                                        )

        fig.update_traces(textposition = 'inside',
                        textinfo = 'percent+label')
                     
        return fig

app.layout = html.Div(
    style={'width': '100%', 'display': 'inline-block', 'padding': 68,'background-color': '#7978b1'},
    children=[
        html.Div(
            children=[
                html.H1(
                    children = "ปริมาณของกลางยาเสพติด",
                    style = {
                        "textAlign" : "center",
                        "background-color": "#f5f5dc",
                        "border": "5px outset #c39797",
                        "color" : "#696969",
                        "margin-top":"40px"
                    },
                )
            ],
            className = "row",
        ),
        
        html.Div(
            children = [
                html.H2(
                    children = "ปีงบประมาณ 2564",
                    style = {
                        "textAlign": "center",
                        "background-color": "#faebd7",
                        "border": "5px outset #cbbeb5",
                        "color": "#808080",
                        "margin-top":"5px"
                    },
                ),
            ],
            className = "row",
        ),

        html.Div(
            [
                html.Div(
                    [
                        dcc.Graph(id = "pie-graph")
                    ],
                    className = "col-8",
                ),
                html.Div(
                    [
                        dbc.Label("Select Year and Narcotics", size= 'lg',style= dict(marginLeft=10, marginTop = 50), color='#ee749b'),
                        dbc.Select(
                            id = "ปีงบประมาณ",
                            value = df["สารเสพติด"].min(),
                            options = [{'label': s, 'value': s} for s in np.sort(df['จังหวัด'].unique())],    
                        ),
                       
                        dbc.Select(
                            id = "จังหวัด",
                            value = df["จังหวัด"][0],
                            options = [
                                dict(label = et.strip(), value = et.strip())
                                for et in df["จังหวัด"].unique()
                            ],
                        ),

                        dbc.Label("Select Chart", size = 'lg', style = dict(marginLeft = 10, marginTop = 20), color = '#ee749b'),
                        dcc.RadioItems(
                            options = [
                                {
                                    'label': html.Div(['Scatter Chart'], style = {'color': '#e5da6c', 'font-size': 18, 
                                                                        "margin-top":"-12px", 'padding-left': 20,}), 
                                    'value': 'scatter'
                                },
                                {
                                    'label': html.Div(['Pie Chart'], style = {'color': '#40ba9e', 'font-size': 18, 
                                                                        "margin-top":"-12px", 'padding-left': 20,}),  
                                    'value': 'pie'
                                }
                            ],
                            value = 'scatter',
                            id = ('choice'), inline = False , labelStyle={'display': 'block','margin-top':'-10px'},
                        ),
                    ],
                    className = "col-4",
                ),
            ],
            className = "row",
        ),
    ],
    className = "container-fluid",
)

        
if __name__ == 'main':
    app.run_server()